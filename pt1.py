#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

# calculate data points according to PT1 controller

# H(s) = K * (1 - e^-(t/T)

# K = 255 (max value)
# t: time iterable
# T = 16 (knee value after iterable)
# supposed to reach maximum after 128 units, to
# have full periodicity after t=255

t = np.arange(0, 128, 1)
h = np.around( 255 * (1 - np.exp(- (t/16))) )
invh = 255 - h

curve = np.concatenate((h, invh))

plt.axis([ -1, 257, -1, 257 ])
plt.plot(np.arange(0, 256, 1), curve)

for i in curve:
    print(f"{int(i)}, ", end="")

plt.show()
