#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

# calculate data points according to sine controller

# H(s) = K * sin(t)^2

# K = 255 (max value
# t: time iterable

# one period of sin(x)^2, which is π-periodic 

t = np.arange(0, 256, 1)
h = np.around( list( map( lambda x: 255 * x * x, np.sin( (t/255) * np.pi) ) ) )

plt.axis([ -1, 257, -1, 257 ])
plt.plot(np.arange(0, 256, 1), h)

for i in h:
    print(f"{int(i)}, ", end="")

plt.show()
